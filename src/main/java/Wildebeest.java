public class Wildebeest extends Animal{
    public Wildebeest() {
        super.name = "Anonymous Wildebeest";
    }

    public Wildebeest(long weight, String name) {
        super.weight = weight;
        super.name = name;
    }

    public Wildebeest(int weight, String name) {
        super.weight = weight;
        super.name = name;
    }
}
